<?php

function feed_image_url () {

    // is this a podcast?
    if(is_tax('podcastfilter')){
        $slug = get_query_var( 'podcastfilter' );
        $term = get_term_by('slug',  $slug,  'podcastfilter');
        $catid = $term->term_id;
    }
    if(!empty( $catid)){
        $image_id =  get_term_meta($catid, 'qt_category_img_id', true);
        if($image_id){
            $image_from_customizer = wp_get_attachment_url ( $image_id, 'thumbnail' ); 
            return $image_from_customizer;
        }
    }
    return 'https://radioblackout.org/wp-content/uploads/2023/03/logo_blackout-trasp1.png';
}

function add_thumbnail ($post_id) {
    if (!has_post_thumbnail($post_id)) { return; }

    $thumbnail_id = get_post_thumbnail_id( $post_id );
    if(empty($thumbnail_id)) { return; }
  
    
    $image_full   = wp_get_attachment_image_src( $thumbnail_id, 'thumbnail');
  
    if ($image_full !== false) {
      printf("<itunes:image href='%s' type='%s'/>\n\n",$image_full[0], get_post_mime_type( $thumbnail_id ));
    }    
}

function get_enclosures ($post_id) {
    // wp adds a post meta enclosure for each each media item from posts
    $tmpEnclosures = get_post_meta($post_id, 'enclosure');

    // blackout website has a plugin that uses this meta to publish media
    $podcastUrl = get_post_meta($post_id, '_podcast_resourceurl');


    if (count($podcastUrl)>0) { 
        $podcastUrl = esc_url( trim( $podcastUrl[0] ) );
    }

    $enclosures = array();
    foreach ( (array) $tmpEnclosures as $enc ) {
        $enclosure = explode( "\n", $enc );
        // Only get the first element, e.g. 'audio/mpeg' from 'audio/mpeg mpga mp2 mp3'.
        $t    = preg_split( '/[ \t]/', trim( $enclosure[2] ) );
        $type = $t[0];
        $enclosure = array(
            'url' => esc_url( trim( $enclosure[0] ) ),
            'length' => absint( trim( $enclosure[1] ) ),
            'type' => esc_attr( $type )
        );
        if (count(array_filter($enclosures, function($value) use ($enclosure) { return $value['url'] == $enclosure['url']; }))) { continue; }
        array_push($enclosures, $enclosure);
    }

    if ($podcastUrl) {
        if (!count(array_filter($enclosures, function($value) use ($podcastUrl) { return $value['url'] == $podcastUrl; }))) {
            array_push($enclosures, array(
                'url' => $podcastUrl,
                'length' => absint( 1000 ),
                'type' => esc_attr( 'audio/mpeg' )                
            ));
        }
    }

    return $enclosures;

}

header( 'Content-Type: ' . feed_content_type( 'rss2' ) . '; charset=' . get_option( 'blog_charset' ), true );

echo '<?xml version="1.0" encoding="' . get_option( 'blog_charset' ) . '"?' . '>';
?>
<rss version="2.0"
	xmlns:content="http://purl.org/rss/1.0/modules/content/"
    xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd"
    xmlns:podcast="https://podcastindex.org/namespace/1.0"    
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:atom="http://www.w3.org/2005/Atom"
	xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
	xmlns:slash="http://purl.org/rss/1.0/modules/slash/">

    <channel>
        <title><?php wp_title_rss(); ?></title>
        <atom:link href="<?php self_link(); ?>" rel="self" type="application/rss+xml" />
        <link><?php bloginfo_rss( 'url' ); ?></link>
        <description><?php bloginfo_rss( 'description' ); ?></description>
        <lastBuildDate><?php echo get_feed_build_date( 'r' ); ?></lastBuildDate>
        <language><?php bloginfo_rss( 'language' ); ?></language>
        <itunes:subtitle>One Station Against The Nation</itunes:subtitle>
        <itunes:author>Radio Blackout</itunes:author>
        <itunes:summary><?php bloginfo_rss( 'description' ); ?></itunes:summary>
        <itunes:owner>
            <itunes:name>Radio Blackout</itunes:name>
            <itunes:email>redazione@radioblackout.org</itunes:email>
        </itunes:owner>
        <itunes:explicit>no</itunes:explicit>
        <itunes:image href="<?php echo feed_image_url(); ?>" />
        <itunes:type>episodic</itunes:type>
        <itunes:category text="News">
        <itunes:category text="Politics"/>
        </itunes:category >
        <podcast:funding url="https://sostieniradioblackout.org">Sostieni Radio Blackout</podcast:funding>

	<sy:updatePeriod>
	<?php
		$duration = 'hourly';

		/**
		 * Filters how often to update the RSS feed.
		 *
		 * @since 2.1.0
		 *
		 * @param string $duration The update period. Accepts 'hourly', 'daily', 'weekly', 'monthly',
		 *                         'yearly'. Default 'hourly'.
		 */
		echo apply_filters( 'rss_update_period', $duration );
	?>
	</sy:updatePeriod>
	<sy:updateFrequency>
	<?php
		$frequency = '1';

		/**
		 * Filters the RSS update frequency.
		 *
		 * @since 2.1.0
		 *
		 * @param string $frequency An integer passed as a string representing the frequency
		 *                          of RSS updates within the update period. Default '1'.
		 */
		echo apply_filters( 'rss_update_frequency', $frequency );
	?>
	</sy:updateFrequency>
	<?php
	/**
	 * Fires at the end of the RSS2 Feed Header.
	 *
	 * @since 2.0.0
	 */
	// do_action( 'rss2_head' );

	while ( have_posts() ) :

		the_post();
        $post_id = get_the_ID();
        $enclosures = get_enclosures($post_id); //) get_post_meta($post_id, 'enclosure');
        $multiple = false;
        if (count($enclosures) == 0) {
            $enclosures = array(false);
        } else if (count($enclosures) > 1){
            $multiple = true;
        }
        
        foreach ( $enclosures as $idx => $enclosure ) {

        ?>

            <item>
            <title><?php echo get_the_title_rss() . ($multiple ? "@$idx" : ''); ?></title>
            <itunes:title><?php echo get_the_title_rss() . ($multiple ? "@$idx" : ''); ?></itunes:title>

            <link><?php the_permalink_rss(); ?></link>

            <dc:creator><![CDATA[<?php the_author(); ?>]]></dc:creator>
            <pubDate><?php echo mysql2date( 'D, d M Y H:i:s +0000', get_post_time( 'Y-m-d H:i:s', true ), false ); ?></pubDate>
            <?php the_category_rss( 'rss2' ); ?>
            <guid isPermaLink="false"><?php echo get_the_guid() . ($multiple ? "@$idx" : ''); ?></guid>

            <?php if ( get_option( 'rss_use_excerpt' ) ) : ?>
                <description><![CDATA[<?php the_excerpt_rss(); ?>]]></description>
                
            <?php else : ?>
                <description><![CDATA[<?php the_excerpt_rss(); ?>]]></description>
                <?php $content = get_the_content_feed( 'rss2' ); ?>
                <?php if ( strlen( $content ) > 0 ) : ?>
                    <content:encoded><![CDATA[<?php echo $content; ?>]]></content:encoded>
                <?php else : ?>
                    <content:encoded><![CDATA[<?php the_excerpt_rss(); ?>]]></content:encoded>
                <?php endif; ?>
            <?php endif; ?>
            <?php
                add_thumbnail($post_id);
                if ($enclosure) {
                    echo '<enclosure url="' . $enclosure['url'] . '" length="' . $enclosure['length'] . '" type="' .$enclosure['type'] . '" />' . "\n";
                }
            ?>
    </item>
	<?php } endwhile; ?>
</channel>
</rss>

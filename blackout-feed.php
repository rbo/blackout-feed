<?php
/*
Plugin Name: Blackout Feed
Description: Fix multiple enclosure feed WP issue, adds itunes meta
Version:     1.0
Author:      lesion
License:  AGPL 3.0

Blackout Feed is free software: you can redistribute it and/or modify it under the
terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the license, or any later version.

Blackout Feed is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Blackout Feed.
If not, see (https://www.gnu.org/licenses/agpl-3.0.html).
*/

defined( 'ABSPATH' ) or die( 'Nope, not accessing this' );

define('WPFEED_DIR', plugin_dir_path(__FILE__));

// This delivers valid feeds, with the correct templates.
remove_all_actions( 'do_feed_rss2' );
add_action( 'do_feed_rss2', function() {
  load_template(WPFEED_DIR . 'feed.php');
})
?>
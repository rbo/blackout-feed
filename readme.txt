=== Blackout Feed ===
Contributors: lesion
Tags: enclosure, feed, media:content, itunes:image, podcast
Requires at least: 4.0
Tested up to: 5.9
Stable tag: 1.4
Requires PHP: 7.0
License: AGPLv3 or later
License URI: https://www.gnu.org/licenses/agpl-3.0.html

Fix multiple enclosure feed WP issue, adds itunes meta

== Description ==
Fix multiple enclosure feed WP issue, adds itunes meta


== Changelog ==

= 1.0 =
* First release
